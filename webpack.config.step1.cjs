const path = require("path")
const CopyPlugin = require("copy-webpack-plugin")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")

// configuration
const config = require("./buildConfig.cjs")

module.exports = {
    mode: config.mode,

    entry: {
        index: "./src/emptyJS.js"
    },

    output: {
        filename: config.fileNames,
        path: config.localDeployAnki,
    },

    optimization: {
        minimize: config.mode === "production",
    },

    resolve: {
        extensions: [".js"],
    },

    module: {
        rules: [
            {
                test: /\.handlebars$/,
                use: [{
                        loader: "handlebars-loader",
                        options: {
                            helperDirs: [path.resolve("./src/handlebars/helpers")]
                        }
                    },
                    "extract-loader",
                    {
                        loader: "html-loader",
                        options: {
                            attributes: {
                                list: [{
                                        tag: "img",
                                        attribute: "src",
                                        type: "src",
                                    },
                                    {
                                        tag: "img",
                                        attribute: "srcset",
                                        type: "srcset",
                                    },
                                    {
                                        tag: "img",
                                        attribute: "data-src",
                                        type: "src",
                                    },
                                    {
                                        tag: "img",
                                        attribute: "data-srcset",
                                        type: "srcset",
                                    },
                                ]
                            },
                            minimize: config.mode !== "production" ? false : {
                                removeComments: true,
                                collapseWhitespace: true,
                                conservativeCollapse: false,
                                keepClosingSlash: false,
                                minifyCSS: true,
                                minifyJS: false,
                                removeAttributeQuotes: true,
                                removeScriptTypeAttributes: true,
                                removeStyleTypeAttributes: true,
                                useShortDoctype: true
                            },
                        }
                    },
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff2?)$/i,
                loader: "file-loader",
                options: {
                    name: config.filePrefix + "[name].[ext]",
                    outputPath: path.relative(config.localDeployAnki, config.deployPath)
                }
            },
        ]
    },

    plugins: [
        new CleanWebpackPlugin(),

        new CopyPlugin([
            { from: "src/html/previewFront.handlebars", to: "production/" },
            { from: "src/html/previewFront.handlebars", to: "recognition/" },
            { from: "src/html/previewFront.handlebars", to: "productionFromImage/" },
            { from: "src/html/previewBack.handlebars", to: "production/" },
            { from: "src/html/previewBack.handlebars", to: "recognition/" },
            { from: "src/html/previewBack.handlebars", to: "productionFromImage/" },
            { from: "src/html/assets/butterflies_small.jpg", to: "../preview/" },
        ], { logLevel: "warn" }),

        new HtmlWebpackPlugin({
            title: "Copy-paste into Anki: Production Front",
            template: require.resolve("./src/html/production/Front.handlebars"),
            filename: "production/Front.handlebars",
            inject: false,
        }),

        new HtmlWebpackPlugin({
            title: "Copy-paste into Anki: Production Back",
            template: require.resolve("./src/html/production/Back.handlebars"),
            filename: "production/Back.handlebars",
            inject: false,
        }),

        new HtmlWebpackPlugin({
            title: "Copy-paste into Anki: Production from Image Front",
            template: require.resolve("./src/html/productionFromImage/Front.handlebars"),
            filename: "productionFromImage/Front.handlebars",
            inject: false,
        }),

        new HtmlWebpackPlugin({
            title: "Copy-paste into Anki: Production from image Back",
            template: require.resolve("./src/html/productionFromImage/Back.handlebars"),
            filename: "productionFromImage/Back.handlebars",
            inject: false,
        }),

        new HtmlWebpackPlugin({
            title: "Copy-paste into Anki: Recognition Front",
            template: require.resolve("./src/html/recognition/Front.handlebars"),
            filename: "recognition/Front.handlebars",
            inject: false,
        }),

        new HtmlWebpackPlugin({
            title: "Copy-paste into Anki: Recognition Back",
            template: require.resolve("./src/html/recognition/Back.handlebars"),
            filename: "recognition/Back.handlebars",
            inject: false,
        }),
    ],
}