const path = require("path")
const os = require("os")
const fs = require("fs")
const ANKI_PROFILE_TEST = "Tests"

let settings = {
    mode: "production", // 'production' or 'development'
    deployToAnki: false, //false means deploy locally; a string represents the name of the profile to deploy to
    localDeployRoot: path.resolve("./_dist/"),
    previewHTMLBaseTag: "../anki/",
    filePrefix: "_lela_",
}

Object.assign(settings, {
    // computed values
    debug: settings.mode === "development",
    localDeployAnki: path.resolve(settings.localDeployRoot, "anki/"),
    localDeployPreview: path.resolve(settings.localDeployRoot, "preview/"),
    fileNames: settings.filePrefix + "[name].min.js"
})

Object.assign(settings, {
    deployPath: settings.deployToAnki
        ? fs.realpathSync(path.resolve("~/ankiProfilesDir/".replace("~", os.homedir()), settings.deployToAnki, "collection.media/"))
        : settings.localDeployAnki //potentially overwritten by the code below
})

Object.assign(settings, {
    cardPirouette: {
        "include_production": true,
        "include_recognition": true,
        "learned": "la pirueta",
        "known": "the pirouette [hint]",
        "Tags": "language_EN-ES now verbs indication_3_pieces_learned indication_gerund_learned",
        "example": "<div>Hace la pirueta muy bien</div>",
        "mnemonic": "peer roulette CSS has properties for specifying the margin for each side of an element",
        "usage_note": "sounds fancy",
        "image": "<img src=\"../preview/butterflies_small.jpg\">",
        "media_credits": "<div>File:Group of ancient hellenistic an roan oil lamps.jpg - Wikipedia. Retrieved February 13, 2020, from https://en.wikipedia.org/wiki/File:Group_of_ancient_hellenistic_an_roan_oil_lamps.jpg</div>",
    },
    cardButterfly: {
        "learned": "la mariposa",
        "known": "the butterfly",
        "Tags": "language_EN-ES now verbs indication_3_pieces_learned indication_gerund_learned",
        "example": "He visto una maroposa bonita.",
        "image": "<img src=\"../preview/butterflies_small.jpg\">",
        "media_credits": "Photo by <strong><a href=\"https://www.pexels.com/@evie-shaffer-1259279?utm_content=attributionCopyText&amp;utm_medium=referral&amp;utm_source=pexels\">Evie Shaffer</a>&nbsp;</strong>from <strong><a href=\"https://www.pexels.com/photo/animal-antenna-biology-butterfly-2395264/?utm_content=attributionCopyText&amp;utm_medium=referral&amp;utm_source=pexels\">Pexels</a></strong>",
        "include_recognition": true,
        "include_production": true,
    },
    cardWithoutLanguageTag: {
        "include_production": true,
        "include_recognition": true,
        "learned": "culpable",
        "known": "guilty",
        "Tags": "",
        "example": "<div>Se siente muy culpable</div>",
        "mnemonic": "culo+pebble",
        "usage_note": "",
        "image": "",
        "media_credits": "",
    },
})

module.exports = settings