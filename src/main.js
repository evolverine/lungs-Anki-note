"use strict"
import Cookies from "js-cookie"
import { CardData, Card, Tag, CONTEXT_REGEXP } from "./include/classes.js"
import { element, VISIBILITY_CLASS, compose, setUpErrorHandling, setInnerHTMLSafe, debug, debugStack,
    toggleVisible, addClass, hasClass, doPolyFills } from "./include/utils.js"

setUpErrorHandling()
doPolyFills()

/**
 * GLOBAL DEFINITIONS
 */
window.LELA_VERSION = "0.2.1" //used by index.js to know whether it's already loaded this module before

// Used to detect when Anki changes the card side or moves to a new card
const observer = new MutationObserver(startIfNoteTypeLela)

const BADGES_DIV = "badges_table"
const KNOWN_LANGUAGE_SPAN_ID = "knownLanguage"
const LEARNED_LANGUAGE_SPAN_ID = "learnedLanguage"
const REQUESTED_LANGUAGE_PROMPT_SPAN_ID = "requestedLanguagePrompt"
export const CONTEXT_HTML_WRAPPER = "<span id=\"generatedContext\" class=\"hidden\">$1</span><a href=\"#\" id=\"contextLink\">(👁)</a>"
export const COOKIE_DETECT_LANGUAGE_CHANGE = "languageStats"
const ID_BOX_PROMPT = "promptBox"
const ID_BOX_ANSWER = "answerBox"
const ID_BOX_ANSWER_PREVIEW = "answerBoxPreview"



/**
 * FUNCTIONS
 */

/**
 * this function runs only the first time;
 * 100 notes/cards might go by without this script
 * being loaded again, and thus this function being called */
function firstRun() {
    document.readyState == "complete" ? setTimeout(start, 0)
        : document.body.onload = start

    observer.observe(document.querySelector("#qa"), {childList: true})
}

function startIfNoteTypeLela() {
    if(element("#lelaFront"))
        start()
}

export function start() {
    let cardData = new CardData(element("#cardData_known").innerHTML, element("#cardData_learned").innerHTML, element("#cardData_tags").textContent)
    let card = new Card(cardData)
    compose(renderContext, recordCurrentLanguageCombination, showBadges, renderLanguagePrompts, themeCard, initCookies, debugCard)(card)
}

function debugCard(card) {
    if(ENV.DEBUG) {
        debug("starting to render card with language '" + card.languageCombination
            + " and other tags " + card.tags
            + "', known '" + card.known
            + "' and learned '" + card.learned
            + "'")
        debugStack()
    }
    return card
}

/**
 * @param {Card} card
 * @return {Card}
 */
function initCookies(card) {
    if(card.isFrontSide) {
        if(!Cookies.get(COOKIE_DETECT_LANGUAGE_CHANGE)) {
            Cookies.set(COOKIE_DETECT_LANGUAGE_CHANGE, {
                previousLanguageTag: "",
                languageCombinationJustChanged: false
            }, { expires: 0.5 })
        }
    }

    return card
}

/**
 * assumes that badges are hidden by default
 * @return {Card}
 */
function showBadges(card) {
    function showBadge(cardTag) {
        let tag = new Tag(cardTag)
        let badge = element("#badge_" + tag.nameWithoutModifiers)
        if(badge) {
            let badgeNotYetProcessed = !(hasClass(badge, VISIBILITY_CLASS[true]) || hasClass(badge, VISIBILITY_CLASS[false]))
            if(badgeNotYetProcessed) {
                let shouldBadgeBeVisible = tag.isAboutBoth ? true : getBadgeVisibility(tag.shouldBeShownWithKnown, card.isAKnownLearnedCard)
                //debug(tag.nameWithoutModifiers + ".visible == " + badge.classList + ", and should be " + shouldBadgeBeVisible)
                if(shouldBadgeBeVisible) {
                    toggleVisible(badge, shouldBadgeBeVisible)
                    badgesShown++
                    onNewVisibleBadge(tag)
                }
            }
        }
    }

    /** @return {string} */
    function getBadgeVisibility(shouldBadgeAppearNextToKnown, weAreOnAKnownLearnedSide) {
        //debug("next to known: " + shouldBadgeAppearNextToKnown + "; knownlearned: " + weAreOnAKnownLearnedSide)
        return weAreOnAKnownLearnedSide === shouldBadgeAppearNextToKnown
    }

    function onNewVisibleBadge(tag) {
        //if it's the 2_piece one, show another message in the preview box
        let previewMessage = null
        for(let i = tag.numPieces - 1; i > 0; i--) {
            previewMessage = previewMessage || element("#previewMessage") // eslint-disable-line no-undef
            previewMessage.insertAdjacentHTML("afterend", previewMessage.cloneNode(true).outerHTML)
        }
    }

    let badgesShown = 0
    card.tags.forEach(showBadge)
    if(badgesShown === 0)
        removeElement(BADGES_DIV)
    return card
}


/** @return {Card} */
function renderLanguagePrompts(card) {
    function setLanguagePrompts(card) {
        if(card.knownLanguage && card.learnedLanguage) {
            setInnerHTMLSafe(REQUESTED_LANGUAGE_PROMPT_SPAN_ID, (card.isAKnownLearnedSide ? card.learnedLanguage : card.knownLanguage))
            setInnerHTMLSafe(LEARNED_LANGUAGE_SPAN_ID, card.learnedLanguage)
            setInnerHTMLSafe(KNOWN_LANGUAGE_SPAN_ID, card.knownLanguage)
        }
    }

    function drawAttentionToRequiredLanguageSpan() {
        element("#" + REQUESTED_LANGUAGE_PROMPT_SPAN_ID).style.setProperty("color", "var(--highlighted-language-prompt-color)")
    }

    /** @return {boolean} */
    function userMightOverlookTheChangeOfLanguages(languageCombination) {
        let userMightOverlookLanguageChanges = false
        let langStats = Cookies.getJSON(COOKIE_DETECT_LANGUAGE_CHANGE)
        if(langStats) {
            let isLanguageCombinationChanging = langStats.previousLanguageTag !== languageCombination
            userMightOverlookLanguageChanges = isLanguageCombinationChanging || langStats.languageCombinationJustChanged
            langStats.languageCombinationJustChanged = isLanguageCombinationChanging

            Cookies.set(COOKIE_DETECT_LANGUAGE_CHANGE, langStats, { expires: 0.5 })

            //debugStatic("#card: " + langStats.counter + "; ratio: " + ratioOfCurrentLanguageCombination);
        }
        return userMightOverlookLanguageChanges
    }

    setLanguagePrompts(card)
    if(userMightOverlookTheChangeOfLanguages(card.languageCombination))
        drawAttentionToRequiredLanguageSpan()

    return card
}

/** @return {Card} */
function recordCurrentLanguageCombination(card) {
    let langStats = Cookies.getJSON(COOKIE_DETECT_LANGUAGE_CHANGE)
    if(langStats) {
        langStats.previousLanguageTag = card.languageCombination
        Cookies.set(COOKIE_DETECT_LANGUAGE_CHANGE, langStats, { expires: 0.5 })
    }

    return card
}

/** @export for testing */
export function insertContextInString(str) {
    return str.replace(CONTEXT_REGEXP, CONTEXT_HTML_WRAPPER)
}

/** @return {Card} */
function renderContext(card) {
    function hideContext(card) {
        if(card.promptText.search(CONTEXT_REGEXP) != -1) {
            let cardPrompt = element("#" + card.promptID)
            cardPrompt.innerHTML = insertContextInString(cardPrompt.innerHTML)
            element("#contextLink").addEventListener("click", showContext)
        }
    }

    function showContext(e) {
        e.preventDefault()
        toggleVisible(element("#generatedContext"), true)
        toggleVisible(element("#contextLink"), false)
    }

    hideContext(card)
    return card
}


/** @return {Card} */
function themeCard(card) {
    if(card.promptLanguage) {
        addClass(ID_BOX_PROMPT, card.promptLanguage)
        const PROMPT_LANGUAGE_ID = card.isAKnownLearnedCard ? KNOWN_LANGUAGE_SPAN_ID : LEARNED_LANGUAGE_SPAN_ID
        addClass(PROMPT_LANGUAGE_ID, card.promptLanguage)
    }

    if(card.answerLanguage) {
        addClass(ID_BOX_ANSWER, card.answerLanguage)
        addClass(ID_BOX_ANSWER_PREVIEW, card.answerLanguage)
        const ANSWER_LANGUAGE_ID = card.isAKnownLearnedCard ? LEARNED_LANGUAGE_SPAN_ID : KNOWN_LANGUAGE_SPAN_ID
        addClass(ANSWER_LANGUAGE_ID, card.answerLanguage)
    }

    return card
}


/** @return void */
function removeElement(id) {
    let e = element("#" + id)
    if(e)
        e.parentNode.removeChild(e)
    return e !== null
}



//var ok = firstRun // eslint-disable-line
firstRun()