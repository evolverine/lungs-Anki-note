/**
 * Glossary:
 *
 * - "explicit tags": indication_x_pieces[_learned/_known]
 * - "implicit tags": indication_singular_plural[_learned/_known], indication_2_genders[_learned/_known]
 *                : called "implicit" because through what they require they suggest a specific number of pieces to be produced (2 for each)
 */

import { element } from "./utils.js"

export const CONTEXT_REGEXP = /(\[.+\])/g

export const ID_LEARNED = "learned"
export const ID_KNOWN = "known"

const LANGUAGE_INFO_PREFIX = "language_"
const FRONT_WRAPPER_ID = "frontOnBack"
const TAG_MOD_ABOUT_LEARNED = "_learned"
const TAG_MOD_ABOUT_KNOWN = "_known"
const TAG_2_GENDERS = "indication_2_genders"
const TAG_SINGULAR_PLURAL = "indication_singular_plural"
const CLARIFICATION_PREFIX = "clarification"
const INDICATION_PREFIX = "indication"
const NUM_PIECES_TAG_PREFIX = INDICATION_PREFIX + "_"
const NUM_PIECES_TAG_SUFFIX = "_pieces"


export class Tag {
    get nameWithoutModifiers() { return this.nameWithoutLearnedAndKnownModifiers }

    get isClarification() { return this.nameWithoutModifiers.indexOf(CLARIFICATION_PREFIX) === 0 }
    get isIndication() { return this.nameWithoutModifiers.indexOf(INDICATION_PREFIX) === 0 }

    get isAboutLearned() { return this.isTagAboutLearned }
    get isAboutKnown() { return this.isBadgeAboutKnown }
    get isAboutBoth() { return this.isAboutLearned === this.isAboutKnown }
    get numPieces() { return Number(this.nameWithoutModifiers.replace(new RegExp(NUM_PIECES_TAG_PREFIX + "([0-9])" + NUM_PIECES_TAG_SUFFIX, "g"), "$1")) }

    get shouldBeShownWithKnown() {
        return this.isAboutBoth || (this.isIndication && this.isAboutLearned) || (this.isClarification && this.isAboutKnown)
    }

    processName() {
        let originalName = this.name
        let tagWithoutLearnedModifier = this.name.replace(new RegExp(TAG_MOD_ABOUT_LEARNED + "$"), "")
        this.isTagAboutLearned = tagWithoutLearnedModifier !== originalName

        this.nameWithoutLearnedAndKnownModifiers = tagWithoutLearnedModifier.replace(new RegExp(TAG_MOD_ABOUT_KNOWN + "$"), "")
        this.isBadgeAboutKnown = this.nameWithoutLearnedAndKnownModifiers !== tagWithoutLearnedModifier
    }

    constructor(name) {
        this.name = name
        this.nameWithoutLearnedAndKnownModifiers = ""
        this.isTagAboutLearned = false
        this.isBadgeAboutKnown = false
        this.processName()
    }
}

export class Card {
    constructor( /** @type {CardData} */ cardData) {
        this.isAKnownLearnedSide = element("#" + ID_LEARNED) && element("#" + ID_LEARNED).classList.contains("hidden")
        this.isFront = element("#" + FRONT_WRAPPER_ID) === null
        /** @type {CardData} */
        this._cardData = cardData

        this._knownLanguage = ""
        this._learnedLanguage = ""
        this._languageCombination = ""
        this._originalTags = null
        this.tags = null

        this.processData()
    }

    get isAKnownLearnedCard() { return this.isAKnownLearnedSide }
    get isFrontSide() { return this.isFront }
    get knownLanguage() { return this._knownLanguage }
    get learnedLanguage() { return this._learnedLanguage }
    get languageCombination() { return this._languageCombination }
    get originalTags() { return this._originalTags }

    get known() { return this._cardData.known }
    get learned() { return this._cardData.learned }
    get promptText() { return this.isAKnownLearnedCard ? this.known : this.learned }
    get promptID() { return this.isAKnownLearnedCard ? ID_KNOWN : ID_LEARNED }
    get promptLanguage() { return this.isAKnownLearnedCard ? this._knownLanguage : this._learnedLanguage }
    get answerLanguage() { return this.isAKnownLearnedCard ? this._learnedLanguage : this._knownLanguage }

    static toTagsArray(tagsString) {
        return (tagsString ? tagsString : "").split(" ").filter(tag => tag.length > 0)
    }

    processData() {
        function detectLanguageTag(tagsArray) {
            return tagsArray.find(tagName => tagName.indexOf(LANGUAGE_INFO_PREFIX) === 0)
        }

        /** @return {knownLanguage:string, learnedLanguage:string} */
        function languageTagToLanguageArray(languageTag) {
            languageTag = languageTag ? languageTag.substr(LANGUAGE_INFO_PREFIX.length) : ""
            const positionOfDash = languageTag.indexOf("-")

            let languages = {}
            languages.knownLanguage = languageTag.substring(0, positionOfDash)
            languages.learnedLanguage = languageTag.substring(positionOfDash + 1)
            return languages
        }

        this._originalTags = Card.toTagsArray(this._cardData.tags)
        this.tags = this.addComputedTags(this._originalTags, this._cardData)
        let languageDef = languageTagToLanguageArray(detectLanguageTag(this.tags))
        this._knownLanguage = languageDef.knownLanguage
        this._learnedLanguage = languageDef.learnedLanguage
        this._languageCombination = this._knownLanguage + "-" + this._learnedLanguage
    }

    /**
     * @param {Array<string>} tagArray
     * @param {CardData} cardData
     * @return {Array<string>} */
    addComputedTags(tagArray, cardData) {
        return tagArray.concat(computeNumberOfPieces(tagArray))

        /** @return {Array<string>} */
        function computeNumberOfPieces(tags) {
            return [numPiecesTagForSide(TAG_MOD_ABOUT_KNOWN, tags), numPiecesTagForSide(TAG_MOD_ABOUT_LEARNED, tags)]
                .filter(tag => tag.length)

            /** @return {string} */
            function numPiecesTagForSide(side, allTags) {
                function other(side) { return side === TAG_MOD_ABOUT_KNOWN ? TAG_MOD_ABOUT_LEARNED : TAG_MOD_ABOUT_KNOWN }

                function createNumPiecesTag(numPieces, side) { if(numPieces > 5) numPieces = "5_plus"; return NUM_PIECES_TAG_PREFIX + numPieces + NUM_PIECES_TAG_SUFFIX + side }

                function tagAppliesToCurrentSide(tag) { return tag.indexOf(side) === tag.length - side.length || tag.indexOf(other(side)) === -1 }

                function pieceTags(tag) { return tag.indexOf("_piece") !== -1 }

                function numPiecesFromPiecesTag(piecesTag) {
                    let tagWithoutIndication = piecesTag.substr(NUM_PIECES_TAG_PREFIX.length)
                    return tagWithoutIndication.substr(0, tagWithoutIndication.indexOf("_"))
                }

                function explicitNumPieces(tags) {
                    var pieceTagsForCurrentSide = tags.filter(pieceTags).filter(tagAppliesToCurrentSide)
                    return pieceTagsForCurrentSide.length ? numPiecesFromPiecesTag(pieceTagsForCurrentSide[0]) : 0
                }

                function createIndicationTagsForSide(tag, about_known_or_learned) {
                    return [tag].concat(about_known_or_learned.length ? [tag + about_known_or_learned] : [])
                }

                function extraTag(piecesInText, explicitPiecesInTags, implicitPiecesInTags) {
                    if(!explicitPiecesInTags && numPiecesInText > implicitPiecesInTags)
                        return createNumPiecesTag(numPiecesInText, side)
                    return ""
                }

                let text = side === TAG_MOD_ABOUT_KNOWN ? cardData.knownWithoutContext : cardData.learnedWithoutContext
                //debug("");
                let numPiecesInText = text.replace(/<br\s*\/?>/ig, "\n").split(/[\n,]/).filter(part => part.length > 0).length

                //debug("SIDE: " + side);
                //debug("num pieces in " + text + ":" + numPiecesInText);

                let numPiecesFromImplicitTags = allTags.intersect(createIndicationTagsForSide(TAG_2_GENDERS, side)).length > 0 ? 2 : 1
                numPiecesFromImplicitTags *= allTags.intersect(createIndicationTagsForSide(TAG_SINGULAR_PLURAL, side)).length > 0 ? 2 : 1
                //debug("num implicit pieces from tags: " + numPiecesFromImplicitTags);
                //debug("num explicit pieces from tags: " + explicitNumPieces(allTags));

                //TODO: turn below comments into unit tests
                //1 piece, and EXplicit and IMplicit tags say nothing, don't add any tag (the default is to produce one piece)
                //1 piece, and EXplicit tags say 1, don't add any tag (that tag is superfluous, but correct)
                //1 piece, and EXplicit tags say 2, don't add any tag ('the user knows what they're doing')
                //1 piece, and IMplicit tags say 2, don't add any tag ('the user knows what they're doing')
                //...
                //2 pieces, and EXplicit and IMplicit tags say nothing, then ADD 2 pieces tag (auto-detecting number of pieces in absence of user-defined tags)
                //2 pieces, and EXplicit tags say 1, don't add any tag (allowing the user to over-ride the auto-detect)
                //2 pieces, and EXplicit tags say 2, don't add any tag (that tag is superfluous, but correct)
                //2 pieces, and IMplicit tags say 2, don't add any tag (the IMplicit tag correctly suggests the number of pieces by itself)
                //2 pieces, and EXplicit tags say 3, don't add any tag ('the user knows what they're doing')
                //2 pieces, and EXplicit tags say 4, don't add any tag ('the user knows what they're doing')
                //2 pieces, and IMplicit tags say 4, don't add any tag ('the user knows what they're doing')
                //...
                //3 pieces, and EXplicit and IMplicit tags say nothing, then ADD 3 pieces tag (auto-detecting number of pieces in absence of user-defined tags)
                //3 pieces, and EXplicit tags say 1, don't add any tags (allowing the user to over-ride the auto-detect)
                //3 pieces, and EXplicit tags say 2, don't add any tags (allowing the user to over-ride the auto-detect)
                //3 pieces, and IMplicit tags say 2, ADD 3 pieces tag (auto-detecting number of pieces when tags suggest fewer)
                //3 pieces, and EXplicit tags say 3, don't add any tags (that tag is superfluous, but correct)
                //3 pieces, and EXplicit tags say 4, don't add any tags ('the user knows what they're doing')
                //3 pieces, and IMplicit tags say 4, don't add any tags ('the user knows what they're doing')
                //...
                //4 pieces, and tags say nothing, then ADD 3+ pieces tag (auto-detecting number of pieces in absence of user-defined tags)
                //4 pieces, and EXplicit tags say 1, then don't add any tags (allowing the user to over-ride the auto-detect)
                //4 pieces, and EXplicit tags say 2, then don't add any tags (allowing the user to over-ride the auto-detect)
                //4 pieces, and IMplicit tags say 2, then ADD 3+ pieces tag (auto-detecting number of pieces when tags suggest fewer)
                //4 pieces, and EXplicit tags say 3, then don't add any tags ('the user knows what they're doing')
                //4 pieces, and EXplicit tags say 4, then don't add any tags (that tag is superfluous, but correct)
                //4 pieces, and IMplicit tags say 4, then don't add any tags (the IMplicit tags correctly suggest the number of pieces by themselves)
                //4 pieces, and EXplicit tags say 5, then don't add any tags ('the user knows what they're doing')
                //...
                //5 pieces, and tags say nothing, then ADD 3+ pieces tag (auto-detecting number of pieces in absence of user-defined tags)
                //5 pieces, and EXplicit tags say 1, then don't add any tags ('the user knows what they're doing')
                //5 pieces, and EXplicit tags say 2, then don't add any tags ('the user knows what they're doing')
                //5 pieces, and IMplicit tags say 2, then ADD 3+ pieces tag (auto-detecting number of pieces when tags suggest fewer)
                //5 pieces, and EXplicit tags say 3, then don't add any tags ('the user knows what they're doing')
                //5 pieces, and EXplicit tags say 4, then don't add any tags ('the user knows what they're doing')
                //5 pieces, and IMplicit tags say 4, then ADD 3+ pieces tag (auto-detecting number of pieces when tags suggest fewer)
                //5 pieces, and EXplicit tags say 5, then don't add any tags (that tag is superfluous, but correct)
                //5 pieces, and EXplicit tags say 6, then don't add any tags ('the user knows what they're doing')
                //5 pieces, and IMplicit tags say 6, then don't add any tags ('the user knows what they're doing')
                //...
                //CONCLUSIONS:
                // 1) existing EXplicit tags "always win", meaning that no new tags are added when they exist
                // 2) conversely, it's only when EXplicit tags are not present that there's the possibility of adding an extra tag

                //debug("...and returning " + extraTag(numPiecesInText, explicitNumPieces(allTags), numPiecesFromImplicitTags));
                return extraTag(numPiecesInText, explicitNumPieces(allTags), numPiecesFromImplicitTags)
            }
        }
    }
}

export class CardData {
    constructor(known, learned, tags) {
        this.known = known
        this.learned = learned
        this.tags = tags
    }

    stripContext(text) {
        return text.replace(CONTEXT_REGEXP, "").trim()
    }

    get knownWithoutContext() { return this.stripContext(this.known) }
    get learnedWithoutContext() { return this.stripContext(this.learned) }
}