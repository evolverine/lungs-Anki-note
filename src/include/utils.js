export const VISIBILITY_CLASS = { true: "visible", false: "hidden" }

/**
 * Composes multiple functions
 * @param {...function(...)} var_args
 * @return {function(...)}
 */
export function compose() {
    let fns = arguments

    return /** @return {Object} */ function(result) {
        for(let i = fns.length - 1; i > -1; i--) {
            result = fns[i].call(this, result)
        }

        return result
    }
}

export function setUpErrorHandling() {
    /** @return {boolean} */
    function traceError(msg, url, lineNo /*, columnNo, error*/ ) {
        debug("ERROR: " + msg + " at line " + lineNo + " in script " + url)
        return false
    }

    /** @return {function()} */
    function proxyLog(context, method) {
        return function() {
            method.apply(context, arguments)
            debug(Array.prototype.join.apply(arguments))
        }
    }

    console.warn = proxyLog(console, console.warn)
    console.exception = proxyLog(console, console.exception)
    window.onerror = traceError
}

export function setInnerHTMLSafe(elementID, innerHTML) {
    let elem = element("#" + elementID)
    if(elem)
        setInnerHTML(elem, innerHTML)
}

function setInnerHTML(element, innerHTML) {
    element.innerHTML = innerHTML
}

export function debugStack() {
    debug(new Error().stack.replace(",", "<br/>"))
}

export function debug(text) {
    appendText(text, "debug")
}

function appendText(text, withinDIVOfClass) {
    let parentDiv = null
    if(withinDIVOfClass) {
        parentDiv = document.createElement("div")
        parentDiv.classList.add(withinDIVOfClass)
        document.body.appendChild(parentDiv)
    }
    (parentDiv ? parentDiv : document.body).appendChild(document.createTextNode(text))
}

/**
 * @param elem
 * @param visible
 * @param {string=} visibilityClass
 */
export function toggleVisible(elem, visible, visibilityClass) {
    if(visible === undefined) {
        visible = elem.style.display === "none" || hasClass(VISIBILITY_CLASS[false])
    }

    if(visibilityClass === undefined) {
        visibilityClass = VISIBILITY_CLASS[visible]
    }

    elem.classList.remove(VISIBILITY_CLASS[!visible])
    elem.classList.add(visibilityClass)
}

export function addClass(elem, className) {
    if (typeof elem == "string")
        elem = element("#" + elem)

    if(elem)
        elem.classList.add(className)
}

export function hasClass(elem, className) {
    return elem && elem.classList.contains(className)
}

export function element(selector) {return document.querySelector(selector)}

export function doPolyFills() {
    // https://tc39.github.io/ecma262/#sec-array.prototype.find
    if(!Array.prototype["intersect"]) {
        Object.defineProperties(Array.prototype, /** @lends {Array.prototype} */ {
            intersect: {
                writable: false,
                value:
                    /**@param {Array} otherArray
                     * @return {Array}
                     * @this {Array}
                     * @suppress {checkTypes} */
                    function(otherArray) {
                        if(!Array.isArray(otherArray)) {
                            throw new TypeError("argument must be an array")
                        }

                        return this.concat().filter(function(item) {
                            return otherArray.indexOf(item) !== -1
                        })
                    }
            }
        })
    }
}