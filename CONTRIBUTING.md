# Contributing

When contributing to this repository, especially when adding new features, it's good practice to discuss the changes you wish to make via [gitlab issues](https://gitlab.com/evolverine/lela-Anki-note/-/issues) before making a change.

Please note we have a [code of conduct](CODE_OF_CONDUCT.md), please follow it in all your interactions with the project.

# Dos and don'ts

## Do

* test your changes by [building locally](#Building-the-source-code), if applicable
* add or edit unit/integration tests for the code you author
* search through the (open and closed) issues to see if the change you want has already been discussed
* open new tickets for the changes you want to make.

## Don't

* make a pull request if your build or tests are failing
* remove unit tests to make the build pass

# Project structure

Some pointers as to the project's organisation:

* all source code and assets are located in the `src` folder
* the files in `src/html` are responsible for generating both
  * the Anki html templates. The build script will generate them in `_dist/anki`
  * and also the html files used for testing outside of Anki. The build script creates them in `_dist/preview`
* the build scripts include
  * [the `build` shell script](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/build) is the recommended way to build the source code, after setting the desired configuration settings in [`buildConfig.cjs`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/buildConfig.cjs)
  * [`buildConfig.cjs`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/buildConfig.cjs), which contains configuration variables such as whether the build should be a debug build, a production vs. development build, or whether to deploy directly to Anki or to the local `deploy` folder.
  * all files named `webpack.config.[1-3].cjs` - webpack configuration files that split the build in 3 parts. ([This is definitely not ideal](#52))
  * configuration files for
    * [eslint](https://eslint.org/) ([`.eslintrc.json`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/.eslintrc.json))
    * [babel](https://babeljs.io/) ([`babel.config.json`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/babel.config.json))
    * [ava](https://github.com/avajs/ava) ([`ava.config.js`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/ava.config.js))
    * [git](https://git-scm.com/) ([`.gitignore`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/.gitignore))
    * [node](https://nodejs.org/) ([`package.json`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/package.json) and [`package-lock.json`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/package-lock.json))

# Contributing

## Getting started

1. [Fork this repository](https://gitlab.com/evolverine/lela-Anki-note/-/forks/new)
1. [Clone your new fork locally](https://gitlab.com/help/gitlab-basics/start-using-git.md#clone-a-repository)
2. [Install node and npm](https://nodejs.org/en/download/), if you haven't already
2. Navigate to this repository's folder on your machine, and run `npm install` from the command line

## Building from source

1. Follow the steps [above](#Getting-started)
2. Edit [`buildConfig.cjs`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/buildConfig.cjs) to make it a development build: set `settings.mode` to `"development"`, while making sure that `settings.deployToAnki` is set to `false`.
3. Still in [`buildConfig.cjs`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/buildConfig.cjs), you can define the Anki note properties that your want the resulting html pages to contain. Search for `cardPirouette` and `cardButterfly`, and change any of the properties you desire; you can also add more cards, if you wish, and change [the build script that uses them](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/webpack.config.step2.cjs).
4. Run [the `build` shell script](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/build) (usually `./build` in the shell). This will also run the integration tests once the build is complete.
5. If all goes well, your `_dist` folder will be populated with the build artifacts. To visually test the results, follow these steps:

## Testing the results in the browser

1. Make sure you've run a successful build (see above).
2. In a browser of your choice, open any of the html pages in `_dist/anki/preview`. This will give you a reasonable idea of how that card would look inside Anki.

## Running unit tests

The unit tests are located in the `test` folder. Running them is easy: execute `npm test` on the command line (from the root of the project).

## Running integration tests

The integration tests are located in the `test/integration` folder. They are run automatically after a build started by executing the `build` script. To run them separately,

1. Make sure you run a build (`npm run build`) without errors (warnings are generally ok)
2. Execute `npm test-integration` on the command line (from the root of the project).

## Testing in Anki

There are four separate components that need to go into Anki if you want to test how the build looks there: the html templates, the CSS code, the javascript files, and the images and fonts required for this note type. Here's how to get them into Anki. Before starting, make sure you've run a successful build (see [above](#Building-from-source)) and also make sure you have located your [Anki `collection.media` folder](https://docs.ankiweb.net/#/files?id=file-locations).

1. For **the html templates** we currently have no choice but to manually copy and paste them into Anki. For this, go into the cards section of a note type, and from the dropdown menu select `1: Production ...`, and then, in the `Front Template` section, copy and paste the contents of the file `_dist/anki/production/Front.handlebars`. In the `Back Template` section copy and paste the contents of the file `_dist/anki/production/Back.handlebars`. Repeat the equivalent steps for `2: Recognition ...` and `3: ProductionFromImage ...`.
2. Same for **the CSS code**, it has to be copied from `src/inline.css` and pasted into the `Styling (shared between cards)` section.
3. **The image and font files** (the files with the `.png`, `.svg` and `.woff` extensions in the `_dist/anki/` folder) can either be copied manually into [the Anki `collection.media` folder](https://docs.ankiweb.net/#/files?id=file-locations) (recommended), or automatically (see below).
4. With **the javascript files** you have the same two options as with the image and font files. The simplest way is to just copy and paste all the files ending in `.js` from the `_dist/anki/` folder to your [Anki `collection.media` folder](https://docs.ankiweb.net/#/files?id=file-locations). The way to do it automatically is discussed below.

## Automatic deployment to Anki

As mentioned above, the javascript source files, the images and fonts can be automatically deployed to [the Anki `collection.media` folder](https://docs.ankiweb.net/#/files?id=file-locations). This is used when one needs to test directly inside Anki (for example because a web browser renders the notes differently than Anki) and many changes need to be made (which would made manual copy-pasting frustrating).

1. The recommendation is to have a separate profile in Anki for testing. Mine is called "Tests", but you can call it anything you like (and then adjust the configuration in the steps below).
2. Open [`buildConfig.cjs`](https://gitlab.com/evolverine/lela-Anki-note/-/blob/master/buildConfig.cjs) and paste the absolute path of your Anki profiles folder in the `deployPath` field.
3. In the same file, set `deployToAnki` to your test Anki profile, e.g. "Tests" (or you can use the existing constant, `ANKI_PROFILE_TEST`)