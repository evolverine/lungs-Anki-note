Lela 0.3 will supercharge your language learning. It is an [Anki](http://ankisrs.net/) note type with many features for learning languages. It is heavily based on JavaScript. (The name comes from "learning languages - Lela".)

[[_TOC_]]

# Requirements

To use this note type in Anki Desktop, install the [Cookie Monster plugin](https://ankiweb.net/shared/info/1501583548). In AnkiDroid it should just work (see [installation steps below](#Installation-and-update)).

# Features

## Cuteness

| Card with animals chatting | The chats can contain images |
| :---: | :------: |
| ![Cards have an IM chat application style, where the chatting parties are animals](https://i.imgur.com/tJNjqO8.png "Card with animals chatting")   | ![Same and with images](https://i.imgur.com/I6f9pH2.png "The chats can contain images too")   |

## Automatic detection of how many of words / items one needs to recall

![Cards can display an automatic indication that one needs to produce two, three, or three-plus words, expressions or sentences](https://i.imgur.com/9KBYpcz.png "Card detecting automatically how many forms one needs to produce")

Commas and new lines are assumed to be the separators, but this can also be overridden - see below.

## Same language, same colours and faces

For decreasing interference when learning multiple languages at once.

## Badge indications

### Produce both singular and plural forms

![Cards can display an indication that one needs to produce both the singular and plural versions of a word](https://i.imgur.com/lUBWyvk.png "Card requiring both the singular and plural versions")

### Produce a literary form

![Cards can display an indication that one needs to produce a literary version of a word](https://i.imgur.com/KsD2tbY.png "Card requiring a literary form")

### Produce two-gendered forms

![Cards can display an indication that one needs to produce both the feminine and masculine versions of a word](https://i.imgur.com/EV5Yuey.png "Card requiring both feminine and masculine versions")

## By using specific tags in Anki for each note, we can obtain:

1. Various **indications** (that can be freely combined):

- the number of words / items ("pieces") to recall, when automatic detection does not suffice: `indication_1_piece`, `indication_1_piece_known` (applies only to the known language content), `indication_2_pieces `, `indication_3_pieces_learned` (applies only to the learned language content), etc.
- whether one needs to produce both genders of an adjective or noun: `indication_2_genders`, `indication_2_genders_known`, `indication_2_genders_learned`
- whether one needs to produce both the singular or plural forms of a noun or adjective: `indication_singular_plural`, `indication_singular_plural_learned`
- whether one needs to produce a formal or informal version of the answer: `indication_formal`, `indication_formal_known`, `indication_formal_learned`
- whether one needs to produce a colloquial version of the answer: `indication_colloquial`, `indication_colloquial_learned`, `indication_colloquial_known`
- whether one needs to produce a literary version of the answer: `indication_literary`, `indication_literary_learned`
- whether one needs to produce an expression: `indication_expression`, `indication_expression_learned`, `indication_expression_known`
- whether one needs to use the subjunctive tense in the answer (eg. "I hope it's worth it" can be correctly translated into Spanish with or without the subjunctive): `indication_use_subjunctive`
- whether the card was verified by a native speaker: `verified`

2. Various **clarifications** for the question part of the card

- whether a word is a specific part of speech - \[verb, gerund verb, adjective, adverb\]: `clarification_adjective_known`, `clarification_adverb_learned`, `clarification_gerund`, `clarification_verb_learned`, etc.
- whether a word represents a concept or another \[number\]. This is very useful when that word can have multiple homonymous meanings, eg. "set" in catalan can either be the number 7, or a "set" as in English: `clarification_number`, `clarification_number_learned`, `clarification_number_known`
- whether the known or learned value is an expression or should be taken literally (eg. "todo el mundo" can literally mean "all the world", or figuratively "everyone"): `clarification_expression`, `clarification_expression_known`, `clarification_expression_learned`

3. **Styling**

3.1 Automatic styling

By default the Lela ships with 5 variations on a main theme, each for a different language: English, Spanish, Catalan, Romanian and Ukrainian. These are applied automatically based on the language tags (see [below](#Installation-and-update)). If you would like me to add new default languages, please [open an issue ticket](https://gitlab.com/evolverine/lela-Anki-note/-/issues/new?issue).

3.2 Custom styling

But you can also override all this and add your own styling for a language that's not yet supported; that, or change anything about the current design. Simply add your own CSS styles in the Anki Card Types Editor. For example, we could style a currently-unsupported language, say Swedish:

```css
.SV .language, .SV.language, .SV .message {background-color: rgba(100, 29, 100, 0.8)}
.SV .sender {content: url('fish.png');} /* you'll have to create this yourself */
```

## And others

- Visual alerting when the next card's language combination switches compared to what you just studied.
- Better hint fields: just surround the hint with \[square brackets\]

# Installation and update

1. Make sure you've installed all the requirements (detailed [above](#Requirements))
1. Download [Install Lela 0.3.apkg](https://gitlab.com/evolverine/lela-Anki-note/uploads/37398412c68fa10a1426f9fc45b5fcbf/Install_Lela_0.3.apkg) and open it with Anki.
2. If you're asked whether you want to import this deck, choose Yes. You will see a new deck in your list, called "Lela 0.3 installation successful".
3. \[optional\] Now you can suspend the three cards in this deck, so you don't see them in reviews. **Make sure to not delete the one note inside this deck. Its purpose is to make sure that the files needed for Lela 0.3 won't ever be marked for deletion by Anki**.
4. If you haven't already, tag all your language notes with a tag of this form: `language\_\[language that you know\]-\[language that you are learning\]`. So far I've been using the convention to write the languages in ALL CAPS, and using their [ISO shorthand](https://www.loc.gov/standards/iso639-2/php/code_list.php). For example, if you are learning Catalan from Spanish, the tag would be `language_ES-CA`; if you are learning Spanish from English, your tag would be `language_EN-ES`, and so on.
5. If you've installed the Lela language learning note for the first time, that's all you have to do. Now you're welcome to create new cards with the "Lela 0.3" note type. However, if you're updating from a previous version, you'll now need to change the note type of your old cards from the previous version to the current one. To learn how to do this, read the section in [the Anki manual](https://apps.ankiweb.net/docs/manual.html) starting with "Change Note Type allows you to".

# Contributing

Do help me make Lela awesome! Go to [the contribution guidelines](CONTRIBUTING.md) for details.

# Copyright

© 2020 Mihai C / @evolverine

# Attribution
Animal face media downloaded from [dreamstime](https://www.dreamstime.com/) as 41c7c0d6 ... opayq.com. [Blue Printed font](https://www.fontspace.com/blueprinted-font-f16801) © [Jake Luedecke](https://www.jakeluedecke.com/).
