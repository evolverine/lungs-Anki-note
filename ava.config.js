export default {
	files: ['test/*', '_dist/**/*'],
	require: [
      "./test/_browser_env.js",
      "./test/_testUtils.js"
    ],
    verbose: true
}