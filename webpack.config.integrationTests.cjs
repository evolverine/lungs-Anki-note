const webpack = require("webpack")
const nodeExternals = require("webpack-node-externals")

const config = require("./buildConfig.cjs")
const configForJSConsumption = {PRODUCTION: JSON.stringify(config.mode == "production"), DEBUG: JSON.stringify(config.debug)}

module.exports = {
    mode: "production",

    entry: {
        test_integrations: "./test/integration/allIntegrationTests.js"
    },

    output: {
        filename: "[name].cjs",
        path: config.localDeployAnki,
    },

    target: "node",
    externals: [nodeExternals()],
    devtool: config.debug ? "source-map" : false,

    module: {
        rules: [{
                test: /\.html$/,
                use: "raw-loader",
            },
        ]
    },

    plugins: [
        new webpack.DefinePlugin({ENV: configForJSConsumption}),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 1})
    ]
}