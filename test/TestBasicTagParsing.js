/**
* Basic Tag Parsing of the Card class
* 
* Assumptions:
* 1. The tags string given by Anki will never contain new lines
*/

import test from "ava"
import {CardData, Card} from "../src/include/classes.js"
import {setUp, tearDown} from "./_testUtils.js"

test.before(t => {
    setUp()
})

test.after.always(t => {
    tearDown()
})


test("test null should result in empty Array", t => {
    t.deepEqual(new Card(new CardData("", "", null)).tags, [], "expecting empty array")
})

test("test undefined should result in empty Array", t => {
    t.deepEqual(new Card(new CardData("", "", undefined)).tags, [], "expecting empty array")
})

test("test empty string should result in empty Array", t => {
    t.deepEqual(new Card(new CardData("", "", "")).tags, [], "expecting empty array")
})

test("test space should result in empty array", t => {
    t.deepEqual(new Card(new CardData("", "", " ")).tags, [], "expecting empty array")
})

test("test multiple spaces should result in empty array", t => {
    t.deepEqual(new Card(new CardData("", "", "     ")).tags, [], "expecting empty array")
})

test("test one tag", t => {
    t.deepEqual(new Card(new CardData("", "", "tag")).tags, ["tag"], "expecting ['tag']")
})

test("test one tag prepended with spaces", t => {
    t.deepEqual(new Card(new CardData("", "", "   tag")).tags, ["tag"], "expecting ['tag']")
})

test("test one tag appended with spaces", t => {
    t.deepEqual(new Card(new CardData("", "", "tag      ")).tags, ["tag"], "expecting ['tag']")
})

test("test one tag prepended and appended with spaces", t => {
    t.deepEqual(new Card(new CardData("", "", "  tag      ")).tags, ["tag"], "expecting ['tag']")
})

test("test two tags, more realistic", t => {
    const tags = "language_ES-CA verbs"
    t.deepEqual(new Card(new CardData("", "", tags)).tags, tags.split(" "))
})

test("test two tags, with spaces", t => {
    const tags = "  language_ES-CA     verbs   "
    t.deepEqual(new Card(new CardData("", "", tags)).tags, ["language_ES-CA", "verbs"])
})

test("test two identical tags, with spaces", t => {
    const tags = "language_ES-CA language_ES-CA"
    t.deepEqual(new Card(new CardData("", "", tags)).tags, tags.split(" "))
})

test("test many tags, realistic", t => {
    const tags = "clarification_adverb_known language_EN-ES clarification_gerund_known gerund beginner clarification_number clarification_verb_learned present catalonia language past adjective clarification_adjective_known clase indication_expression_known indication_2_genders_learned indication_number_learned"
    t.deepEqual(new Card(new CardData("", "", tags)).tags, tags.split(" "))
})