import test from "ava"
import {setUp, tearDown, cA,
    cardEnterrar, cardMientrasTanto, cardSummit,
    INDICATION_2_PIECES_KNOWN,
    INDICATION_3_PIECES_KNOWN, INDICATION_3_PIECES_LEARNED} from "./_testUtils.js"

const INDICATION_SINGULAR_PLURAL = "indication_singular_plural"
const INDICATION_SINGULAR_PLURAL_LEARNED = "indication_singular_plural_learned"
const INDICATION_SINGULAR_PLURAL_KNOWN = "indication_singular_plural_known"

const INDICATION_2_GENDERS = "indication_2_genders"
const INDICATION_2_GENDERS_LEARNED = "indication_2_genders_learned"
const INDICATION_2_GENDERS_KNOWN = "indication_2_genders_known"

test.before(t => {
    setUp()
})

test.after.always(t => {
    tearDown()
})

test("test with one word and a gender tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardEnterrar(INDICATION_2_GENDERS_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with one word and a singular plural tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardEnterrar(INDICATION_SINGULAR_PLURAL_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with one word and a gender tag plus a singular plural tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardEnterrar(INDICATION_SINGULAR_PLURAL_KNOWN + " " + INDICATION_2_GENDERS_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with 2 words and a gender tag, expecting no change - 'the implicit tags already suggest the number of pieces correctly'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_2_GENDERS)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with 2 words and a singular plural tag, expecting no change - 'the implicit tags already suggest the number of pieces correctly'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_SINGULAR_PLURAL)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with 2 words and a singular plural tag on one side, expecting the 2 pieces tag on the other", t => {
    //given
    const card = cardMientrasTanto(INDICATION_SINGULAR_PLURAL_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_2_PIECES_KNOWN])), "a 2-piece tag should have been added to the known side")
})

test("test with 2 words and a gender tag plus a singular plural tag, expecting no change - 'the implicit tags already suggest the number of pieces correctly'", t => {
    //given
    const card = cardMientrasTanto(INDICATION_SINGULAR_PLURAL + " " + INDICATION_2_GENDERS_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with 3 words and a gender tag, expecting a 3-piece tag - 'implicit tags don't suffice'", t => {
    //given
    const card = cardSummit(INDICATION_2_GENDERS)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_KNOWN, INDICATION_3_PIECES_LEARNED])), "3-piece tags should have been added")
})

test("test with 3 words and a singular plural tag, expecting 3-piece tags - 'implicit tags don't suffice'", t => {
    //given
    const card = cardSummit(INDICATION_SINGULAR_PLURAL)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_KNOWN, INDICATION_3_PIECES_LEARNED])), "3-piece tags should have been added")
})

test("test with 3 words and a singular plural tag on one side, expecting 3-piece tags - 'implicit tags don't suffice'", t => {
    //given
    const card = cardSummit(INDICATION_SINGULAR_PLURAL_KNOWN)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_KNOWN, INDICATION_3_PIECES_LEARNED])), "3-piece tags should have been added")
})

test("test with 3 words and a singular plural tag plus a gender tag, expecting no change - 'the user knows what they're doing'", t => {
    //given
    const card = cardSummit(INDICATION_SINGULAR_PLURAL + " " + INDICATION_2_GENDERS)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags), "no tags should have been added or removed")
})

test("test with 3 words and a singular plural tag plus a gender tag on one side, expecting 3-piece tag on the other side - 'implicit tags don't suffice'", t => {
    //given
    const card = cardSummit(INDICATION_SINGULAR_PLURAL_LEARNED + " " + INDICATION_2_GENDERS_LEARNED)

    //then
    t.deepEqual(cA(card.tags), cA(card.originalTags.concat([INDICATION_3_PIECES_KNOWN])), "a 3-piece tag should have been added on the known side")
})