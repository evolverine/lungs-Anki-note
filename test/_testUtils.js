import {doPolyFills} from "../src/include/utils.js"
import {Card, CardData} from "../src/include/classes.js"

export const INDICATION_1_PIECES = "indication_1_pieces"
export const INDICATION_1_PIECES_LEARNED = "indication_1_pieces_learned"
export const INDICATION_1_PIECES_KNOWN = "indication_1_pieces_known"

export const INDICATION_2_PIECES = "indication_2_pieces"
export const INDICATION_2_PIECES_LEARNED = "indication_2_pieces_learned"
export const INDICATION_2_PIECES_KNOWN = "indication_2_pieces_known"

export const INDICATION_3_PIECES = "indication_3_pieces"
export const INDICATION_3_PIECES_LEARNED = "indication_3_pieces_learned"
export const INDICATION_3_PIECES_KNOWN = "indication_3_pieces_known"

export const INDICATION_4_PIECES = "indication_4_pieces"
export const INDICATION_4_PIECES_LEARNED = "indication_4_pieces_learned"
export const INDICATION_4_PIECES_KNOWN = "indication_4_pieces_known"

export const INDICATION_5_PIECES = "indication_5_pieces"
export const INDICATION_5_PIECES_LEARNED = "indication_5_pieces_learned"
export const INDICATION_5_PIECES_KNOWN = "indication_5_pieces_known"

export const INDICATION_5_PLUS_PIECES = "indication_5_plus_pieces"
export const INDICATION_5_PLUS_PIECES_LEARNED = "indication_5_plus_pieces_learned"
export const INDICATION_5_PLUS_PIECES_KNOWN = "indication_5_plus_pieces_known"

export function setUp() {
    doPolyFills()

    //TODO move this check out of the Card class
    let knownSpan = document.createElement("SPAN")
    knownSpan.id = "learned"
    document.body.appendChild(knownSpan)
}

export function tearDown() {
    document.innerHTML = ""
}


//"CompareArray": make tags Array comparable by ordering them alphabetically and joining them into a string
export function cA(tagsArray)
{
    return tagsArray.sort()
}

//the production method used for parsing a tags string into an array
function toTArray(tagString)
{
    return Card.prototype.toTagsArray(tagString)
}

//make tags String comparable by converting them into an Array and then applying the method at cA()
function cS(tagsString)
{
    return cA(toTArray(tagsString))
}


//preset cards
export function cardEnterrar(tagsAddition, tags = "language_ES-CA verbs") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData("enterrar", "soterrar", tags))
}

export function cardMientrasTanto(tagsAddition, tags = "adverb indication_expression language_EN-ES now") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData("in the meantime, (in the) meanwhile", "mientras tanto, entretanto", tags))
}

export function cardSummit(tagsAddition, tags = "Marked language_EN-ES noun now") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData(`
        the summit,
        the peak,
        the pinnacle`, `la cumbre, el culmen,
        la cima`, tags))
}

export function cardLazo(tagsAddition, tags = "language_EN-ES noun") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData("the knot, the bow, the bond, the tie", "el lazo", tags))
}

export function cardCinco(tagsAddition, tags = "language_EN-ES noun") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData("the one, the two, the three, the four, the five", "el x", tags))
}

export function cardSixThree(tagsAddition, tags = "language_EN-ES noun") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData("the one, the two, the three, the four, the five, the six", "el one", tags))
}

export function cardToMiss(tagsAddition, tags = "indication_expression_learned language_EN-ES now verbs") {
    tags = tagsAddition !== undefined ? tags + " " + tagsAddition : tags
    return new Card(new CardData("to miss [💏💌]", "echar de menos, extrañar", tags))
}