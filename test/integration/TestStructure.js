import test from "ava"
import jsdom from "jsdom"

import htmlProductionFront from "../../_dist/preview/productionFront.html"
import htmlProductionBack from "../../_dist/preview/productionBack.html"
import htmlImageProductionFront from "../../_dist/preview/productionImageFront.html"
import htmlImageProductionBack from "../../_dist/preview/productionImageBack.html"
import htmlRecognitionFront from "../../_dist/preview/recognitionFront.html"
import htmlRecognitionBack from "../../_dist/preview/recognitionBack.html"

import buildConfig from "../../buildConfig.cjs"

const { JSDOM } = jsdom

const ROOT_FRONT = "#lelaFront"
const ROOT_BACK = "#lelaBack"
const KNOWN = "#known"
const LEARNED = "#learned"
const LEARNED_ANSWER = "#learnedAnswer"
const KNOWN_ANSWER = "#knownAnswer"
const ANKI_CARD_PARENT = "#qa"

test("production front has expected structure", t => {
	const document = workOn(htmlProductionFront)
    
    testIsWrappedByExpectedRootElement(t, document, ROOT_FRONT)
    t.deepEqual(document.querySelector(KNOWN).textContent.trim(), buildConfig.cardPirouette.known, "known text appears on production front")
})

test("production back has expected structure", t => {
    const document = workOn(htmlProductionBack)
    
    testIsWrappedByExpectedRootElement(t, document, ROOT_BACK)
    t.deepEqual(document.querySelector(LEARNED_ANSWER).textContent.trim(), buildConfig.cardPirouette.learned, "learned text appears on production back")
    t.truthy(document.querySelector(ROOT_FRONT))
})

test("recognition front has expected structure", t => {
	const document = workOn(htmlRecognitionFront)

    testIsWrappedByExpectedRootElement(t, document, ROOT_FRONT)
    t.deepEqual(document.querySelector(LEARNED).textContent.trim(), buildConfig.cardWithoutLanguageTag.learned, "learned text appears on recognition front")
})

test("recognition back has expected structure", t => {
    const document = workOn(htmlRecognitionBack)

    testIsWrappedByExpectedRootElement(t, document, ROOT_BACK)
    t.deepEqual(document.querySelector(KNOWN_ANSWER).textContent.trim(), buildConfig.cardButterfly.known, "known text appears on recognition back")
    t.truthy(document.querySelector(ROOT_FRONT))
})

test("image production front has expected structure", t => {
    const document = workOn(htmlImageProductionFront)

    testIsWrappedByExpectedRootElement(t, document, ROOT_FRONT)
    t.regex(document.querySelector(KNOWN).innerHTML, new RegExp(buildConfig.cardButterfly.image, "g"), "image text appears on image production front front")
})

test("image production back has expected structure", t => {
	const document = workOn(htmlImageProductionBack)

    testIsWrappedByExpectedRootElement(t, document, ROOT_BACK)
    t.deepEqual(document.querySelector(LEARNED_ANSWER).textContent.trim(), buildConfig.cardButterfly.learned, "learned text appears on image production back")
    t.truthy(document.querySelector(ROOT_FRONT))
})

function testIsWrappedByExpectedRootElement(t, document, rootElementSelector) {
    const rootWrapper = document.querySelector(rootElementSelector)
    t.truthy(rootWrapper, "the card has to contain a root element")

    const ankiCardParent = document.querySelector(ANKI_CARD_PARENT)
    t.truthy(ankiCardParent)

    t.is(rootWrapper.parentElement, ankiCardParent, "the card root has to be the child of the " + ANKI_CARD_PARENT + " element")
    t.is(ankiCardParent.childElementCount, 1, "the card root has to be the *only* child of the " + ANKI_CARD_PARENT + " element")
}

function workOn(html) {
    const { window } = new JSDOM(html)
    const { document } = window
    return document
}