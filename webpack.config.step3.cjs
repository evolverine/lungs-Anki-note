const path = require("path")
const CopyPlugin = require("copy-webpack-plugin")
const FileManagerPlugin = require("filemanager-webpack-plugin")

const config = require("./buildConfig.cjs")

module.exports = {
    mode: config.mode,

    entry: {
        empty: "./src/emptyJS.js"
    },

    output: {
        filename: "empty.min.js",
        path: config.localDeployRoot,
    },

    resolve: {
        extensions: [".js"],
    },


    plugins: [
        new CopyPlugin([{
            from: "anki/**/+(Front|Back).handlebars",
            to: "",
            context: config.localDeployRoot,
            force: true,
            transform(content/*, path*/) {
                //make it Anki-ready
                return String(content)
                    .replace(/{{{/g, "{{")
                    .replace(/}}}/g, "}}")
                    .replace(/{{#if ([\w$_0-9]+)}}([\s\S]*?){{\/if}}/gm, "{{#$1}}$2{{/$1}}")
                    .replace(/{{> Front}}/g, "{{FrontSide}}")
            },
        }, ], { logLevel: "warn" }),

        new FileManagerPlugin({
            onEnd: {
                delete: [
                    path.join(config.localDeployRoot, "**/preview*.handlebars"),
                    path.join(config.localDeployRoot, "**/empty.min.js"),
                ],
            }
        }),
    ],
}